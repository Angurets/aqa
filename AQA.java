public class AQA {

	public static void main(String[] args) {
		// First task
		System.out.println("Hello, world!");

	}

}


Birds
package Task4;

public interface Birds {

    public void fly();

}


Pigion
package Task4;

public abstract class Pigion implements Birds {
    @Override
    public void fly() {

    }

    public abstract void eatSeeds();
}


Pi
package Task4;

public class PigionDerek extends Pigion{
    @Override
    public void fly() {
        super.fly();
    }

    @Override
    public void eatSeeds() {

    }
}
